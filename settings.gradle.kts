pluginManagement {
    plugins {
        kotlin("jvm") version extra.get("kotlinVersion") as String
        id("io.quarkus") version extra.get("quarkusVersion") as String
    }
}

rootProject.name = "demo-quarkus-aws-lambda"
