plugins {
    kotlin("jvm")
    id("io.quarkus")
}

repositories {
    mavenCentral()
}

dependencies {
    val kotlinVersion: String by project
    val quarkusVersion: String by project

    implementation(platform("org.jetbrains.kotlin:kotlin-bom:$kotlinVersion"))
    implementation(platform("io.quarkus:quarkus-bom:$quarkusVersion"))

    implementation(kotlin("stdlib"))
    implementation("io.quarkus:quarkus-amazon-lambda")
    implementation("io.quarkus:quarkus-kotlin")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
}

tasks.withType<io.quarkus.gradle.tasks.QuarkusBuild> {
    isUberJar = true
}

tasks.withType<io.quarkus.gradle.tasks.QuarkusNative> {
    dockerBuild = "true"
    isEnableHttpUrlHandler = true
}

tasks.register<Zip>("zipNativeImage") {
    group = "native"
    dependsOn += "buildNative"
    
    val quarkus = project.extensions.getByType<io.quarkus.gradle.QuarkusPluginExtension>()
    from("${project.buildDir}/${quarkus.finalName()}-runner") {
        rename { "bootstrap" }
    }
}
