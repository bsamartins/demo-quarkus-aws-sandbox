package io.bsamartins.demo.quarkus.aws.lambda

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import javax.inject.Named

@Named("hello-function")
class HelloFunction: RequestHandler<HelloRequest, HelloResponse> {
    override fun handleRequest(input: HelloRequest, context: Context): HelloResponse {
        return HelloResponse(message = "Hello ${input.name}")
    }
}

class HelloRequest {
    lateinit var name: String
}

data class HelloResponse(val message: String)
